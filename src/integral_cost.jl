
function integral_cost(model::Function, target::Function, xrange, max_stepsize=0.1)
    area::Float64 = 0.
    t::Float64 = xrange[1]
    while t < xrange[2]
        dt = min(max_stepsize, xrange[2] - t)
        area += dt * abs(model(t+dt/2) - target(t+dt/2))
        t += dt
    end
    return area
end

function integral_cost(model::Function, x_data::AbstractArray, y_data::AbstractArray, max_stepsize=0.1)
    @assert issorted(x_data) "integral_cost requires the data to be sorted along the x-axis."
    @assert length(x_data) == length(y_data)

    area::Float64 = 0.
    t::Float64 = x_data[1]
    for i in 2:length(x_data)
        while t < x_data[i]
            dt = min(max_stepsize, x_data[i] - t)
            area += dt * abs(model(t+dt/2) - data_interpolation(t+dt/2, dt, (x_data[i-1], x_data[i]), (y_data[i-1], y_data[i])))
            t += dt
        end
    end
    return area
end

data_interpolation(t, dt, t_range, y_range) = y_range[1] + (y_range[2] - y_range[1]) / (t_range[2] - t_range[1]) * (t - t_range[1])


"""
    normalized_integral_cost(model, x_data, y_data, max_stepsize=0.1)

Return integral_cost, but normalized by the integral of the data iterpolation.
"""
# function normalized_integral_cost(model::Function, x_data, y_data, max_stepsize=0.1)
function normalized_integral_cost(model::Function, args...)
    mismatch_area = integral_cost(model, args...)
    data_area = data_integral(args...)
    return mismatch_area / data_area
end

"""
    data_integral(x_data, y_data)

Return the absolute integral over a linear interpolation of the data.
If the interval crosses zero, then it is split in two.
"""
function data_integral(x_data, y_data, args...)
    area::Float64 = 0.
    for i in 1:length(x_data)-1
        if sign(y_data[i+1]) * sign(y_data[i]) == -1
            (x1, x2) = (x_data[i], x_data[i+1])
            (y1, y2) = (y_data[i], y_data[i+1])

            θ = acos( (x2 - x1) / sqrt(( x2 - x1 )^2 + ( y2 - y1 )^2) )
            length_to_zero = θ == 0 ? zero(typeof(x1)) : y1 / tan(θ)

            area += abs( length_to_zero * (y_data[i])/2)
            area += abs( (x_data[i+1] - x_data[i] - length_to_zero) * (y_data[i+1])/2)
        else
            area += abs((x_data[i+1] - x_data[i]) * (y_data[i]+y_data[i+1])/2)
        end
    end
    return area
end

function data_integral(target::Function, xrange, stepsize)
    area::Float64 = 0.
    x_prev=0.
    y_prev = target(x_prev)
    for x in range(xrange[1], stop=xrange[2], step=stepsize)
        y = target(x)

        if sign(y) * sign(y_prev) == -1
            θ = acos( (x_prev - x) / sqrt(( x_prev - x )^2 + ( y_prev - y )^2) )
            length_to_zero = θ == 0 ? zero(typeof(x)) : y / tan(θ)

            area += abs( length_to_zero * y_prev / 2)
            area += abs( (x - x_prev - length_to_zero) * y / 2)
        else
            area += abs((x - x_prev) * (y_prev + y) / 2)
        end

        x_prev = copy(x)
        y_prev = copy(y)
    end
    return area
end
