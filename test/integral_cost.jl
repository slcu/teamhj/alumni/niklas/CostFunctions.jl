using CostFunctions
using Test
zero_model(t) = 0.

triangle = [0. 0.
            1. 1.
            2. 2.
            3. 3.
            4. 2.
            5. 1.
            6. 0.]

triangle_area = 9.

t_data = triangle[:, 1]
y_data = triangle[:, 2]

@test integral_cost(zero_model, t_data, y_data) ≈ triangle_area
@test integral_cost(x->1.0, t_data, y_data) ≈ 5.
@test integral_cost(x->2.0, t_data, y_data) ≈ 5.
@test integral_cost(x->3.0, t_data, y_data) ≈ 9.

@test integral_cost(x->0., range(0, stop=2pi, length=1000), sin.(range(0, stop=2pi, length=1000)), 1e-4) ≈ 4 atol=1e-4

gamma_pdf(t, r, n)= r^n*t^(n-1)*exp(-r*t)/gamma(n)
@test integral_cost(x -> gamma_pdf(x, 1.5, 3), 0.:0.01:100., fill(0., 10001)) ≈ 1
@test integral_cost(x -> gamma_pdf(x, 1.5, 3), 0.:10.:100., fill(0., 11), 1e-2) ≈ 1
x_range = 0.:0.001:1.
@test integral_cost(x -> gamma_pdf(x, 1.5, 3), x_range, [gamma_pdf(x, 1.5, 3) for x in x_range], 1e-2) ≈ 0 atol=1e-5

@test integral_cost(x -> gamma_pdf(x, 1.5, 3), 0.:10.:100., fill(0., 11)) ≈ 1 atol=1e-5

#### normalized_integral_cost

@test normalized_integral_cost(x->0.0, t_data, y_data) ≈ 1.
@test normalized_integral_cost(x -> gamma_pdf(x, 1.5, 3), 0.:10.:100., fill(0., 11), 1e-2) ≈ Inf
normalized_integral_cost(x -> gamma_pdf(x, 1.5, 3), 0.:10.:100., fill(0.001, 11), 1e-2)

x_range = 0.:0.001:1.
@test normalized_integral_cost(x -> gamma_pdf(x, 1.5, 3), x_range, [gamma_pdf(x, 1.5, 3) for x in x_range]) ≈ 0 atol=1e-6

@test normalized_integral_cost(x -> 0., 1:10, 1:10, 1e-1) ≈ 1.

@test normalized_integral_cost(x->0., range(0, stop=2pi, length=1000), sin.(range(0, stop=2pi, length=1000)), 1e-4) ≈ 1

@test CostFunctions.data_integral([0., 3.], [2., -1.]) ≈ 2.5

@test CostFunctions.data_integral([0., 1., 2., 3.], [0., -1e-70, 1e-38, 1.]) ≈ 0.5

@test CostFunctions.data_integral([0., 1.], [1., -2.]) ≈ 5. /6
@test CostFunctions.data_integral([0., 1.], [1., -1.]) ≈ 1. /2
@test CostFunctions.data_integral([0., 1.], [1., -0.5]) ≈ 5. /12
@test CostFunctions.data_integral([0., 1., 2, 3], [1, 1e-40, -1e-40, 1e-40]) ≈ 1. /2
